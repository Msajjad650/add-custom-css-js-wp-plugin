<?php
/*
Plugin Name: Custom CSS JS
Plugin URI: phlox.solutions
Description: Add Custom CSS/JS custom rules to any single page or post or globally
Version: 1.0.0
Author: Phlox
Author URI: phlox.solutions
License: MIT License
*/

defined('ABSPATH') or die('Access Denied');

define('CUSTOM_CSSJS_PATH', plugin_dir_path(__FILE__));
define('CUSTOM_CSSJS_URL', plugin_dir_url( __FILE__ ));
define('CUSTOM_CSSJS', plugin_basename( __FILE__ ));

class Custom_CSS_JS {
	public function __construct() {
		add_action( 'admin_init', array( &$this, 'registerSettings' ) );
		add_action( 'admin_menu', array( &$this, 'adminPanelsAndMetaBoxes' ) );
		add_action('add_meta_boxes', array(&$this, 'add_custom_box'));
		add_action('save_post', array(&$this, 'save_post'));

		add_action('admin_enqueue_scripts', array(&$this, 'admin_enqueue_scripts_plugin'));
		//add_action('wp_enqueue_scripts', array(&$this, 'admin_wp_enqueue_scripts_plugin'));

		add_action('wp_head', array(&$this, 'wp_header_css_jss'));
		add_action( 'wp_footer', array(&$this, 'wp_footer_css_jss'));

		add_option('custom_css_links', '', false, true);
		add_option('custom_css_rules', '', false, true);
		add_option('custom_js_links', '', false, true);
		add_option('custom_js_rules', '', false, true);
	}

	function admin_enqueue_scripts_plugin(){
		wp_enqueue_style('custom-css-js.css', plugins_url('assets/style.css', __FILE__));
		wp_enqueue_style('phlox-codemirror-css', plugins_url('assets/lib/codemirror.css', __FILE__));
		wp_enqueue_script('phlox-codemirror-js', plugins_url('assets/lib/codemirror.js', __FILE__ ));
		wp_enqueue_script('phlox-codemirror-editor-css', plugins_url('assets/mode/css/css.js', __FILE__ ));
		wp_enqueue_script('phlox-codemirror-editor-js', plugins_url('assets/mode/javascript/javascript.js', __FILE__ ));
	}
	/*function admin_wp_enqueue_scripts_plugin(){
		
	}*/

	function registerSettings() {
		register_setting( 'custom_css_js', 'global_custom_header', 'trim' );
		register_setting( 'custom_css_js', 'global_custom_footer', 'trim' );
	}

	function adminPanelsAndMetaBoxes() {
    	add_submenu_page( 'options-general.php', 'Custom CSS JS', 'Custom CSS JS', 'manage_options', 'custom_css_js', array( &$this, 'adminPanel' ) );
	}

	function adminPanel() {
		if ( !current_user_can( 'administrator' ) ) {
			echo '<p>' . __( 'Sorry, you are not allowed to access this page.', 'custom_css_js' ) . '</p>';
			return;
		}

    	echo '<div class="wrap">';
		echo '<h2>Custom CSS/JS</h2>';
		
		if (!current_user_can( 'manage_options' ))  {
			wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
		}
		if (isset($_POST['custom_css_js_phlox'])) {
			if (!wp_verify_nonce($_POST['custom_css_js_phlox'], plugin_basename( __FILE__ ))) {
				echo 'Could not save, please login and try again.';
			} else {
				$css_header = wp_unslash($_POST['custom_css_header_rules']);
				$css_footer = wp_unslash($_POST['custom_css_footer_rules']);
				$js_header = wp_unslash($_POST['custom_js_header_rules']);
				$js_footer = wp_unslash($_POST['custom_js_footer_rules']);
				
				update_option('custom_css_header_rules', $css_header, true);
				update_option('custom_css_footer_rules', $css_footer, true);
				update_option('custom_js_header_rules', $js_header, true);
				update_option('custom_js_footer_rules', $js_footer, true);
			}
		}
		
		echo '<form method="post" action="">';
		echo '<br /><h3>CSS/JS </h3><br />';
		$this->inner_custom_box();
		echo '<div align="right"><button type="submit" class="button button-primary button-large">Save</button></div>';
		echo '</form>';
		
		echo '</div>';
    }

    function inner_custom_box($post = null) {
		wp_nonce_field(plugin_basename( __FILE__ ), 'custom_css_js_phlox');
		self::generic_box($post, 'css');
		self::generic_box($post, 'js');
	}

	function generic_box($post, $type) {		
		if ($post != null) {
			$links = get_post_meta($post->ID, 'custom_'.$type.'_header_rules', true);
			$rules = get_post_meta($post->ID, 'custom_'.$type.'_footer_rules', true);
		} else {
			$links = get_option('custom_'.$type.'_header_rules', true);
			$rules = get_option('custom_'.$type.'_footer_rules', true);
		}
		$upper = strtoupper($type);
		if ($type == 'css') {
			
			$ptag = 'Please add &lt;style&gt; tags. You can also add CSS links tag (&lt;link rel="stylesheet" href="www.example.com/style.css" type="text/css" /&gt;)<br><br><br>';
		}else{
			
			$ptag = 'Please add &lt;script&gt; tags. You can also add JS links tag (&lt;script type="text/javascript" src="www.example.com/script.js"&gt;&lt;/script&gt;)<br><br><br>';	
		}
		echo '<div class="row"><label for="custom-'.$type.'-rules">Write your custom header '.$upper.'</label><br /><textarea class="css-js-files-text" id="custom_'.$type.'_header_rules" name="custom_'.$type.'_header_rules">'.htmlentities($rules).'</textarea>'.$ptag.'</div>';
		echo '<div class="row"><label for="custom_-'.$type.'-rules">Write your custom footer '.$upper.'</label><br /><textarea class="css-js-files-text" id="custom_'.$type.'_footer_rules" name="custom_'.$type.'_footer_rules">'.htmlentities($links).'</textarea>'.$ptag.'</div>';
		if ($type == 'css') {
			echo '<script type="text/javascript">
			  	var editor = CodeMirror.fromTextArea(document.getElementById("custom_css_header_rules"), {
			    	lineNumbers: true,
			    	mode: "css",
			    	matchBrackets: true,
			    	autoCloseBrackets:true
			    });
			    var editor = CodeMirror.fromTextArea(document.getElementById("custom_css_footer_rules"), {
			    	lineNumbers: true,
			    	mode: "css",
			    	matchBrackets: true,
			    	autoCloseBrackets:true
			    });
			  </script>';	
		}else{
			echo '<script type="text/javascript">
			var editor = CodeMirror.fromTextArea(document.getElementById("custom_js_header_rules"), {
			    lineNumbers: true,
			    mode: "javascript",
			    matchBrackets: true,
			    autoCloseBrackets:true
			});
			
			var editor = CodeMirror.fromTextArea(document.getElementById("custom_js_footer_rules"), {
			    lineNumbers: true,
			    mode: "javascript",
			    matchBrackets: true,
			    autoCloseBrackets:true
			});
			</script>';
		}
	}

	function add_custom_box() {
		$screens = array( 'post', 'page' );
		foreach ($screens as $screen) {
			add_meta_box(
				'css-js-files',
				__('Write your custom CSS/JS', 'custom_css_js'),
				array('Custom_CSS_JS', 'inner_custom_box'),
				$screen
			);
		}
	}

	function save_post($post_id) {
		if ('page' == $_POST['post_type']) {
			if (!current_user_can( 'edit_page', $post_id)) {
				return;
			}
		} else {
			if (!current_user_can('edit_post', $post_id)) {
				return;
			}
		}
		
		if (!isset($_POST['custom_css_js_phlox']) || !wp_verify_nonce($_POST['custom_css_js_phlox'], plugin_basename( __FILE__ ))) {
			return;
		}
		
		$post_ID = $_POST['post_ID'];
		$css_header = $_POST['custom_css_header_rules'];
		$css_footer = $_POST['custom_css_footer_rules'];
		$js_header = $_POST['custom_js_header_rules'];
		$js_footer = $_POST['custom_js_footer_rules'];
		
		add_post_meta($post_ID, 'custom_css_header_rules', $css_header, true) or update_post_meta($post_ID, 'custom_css_header_rules', $css_header);		
		add_post_meta($post_ID, 'custom_css_footer_rules', $css_footer, true) or update_post_meta($post_ID, 'custom_css_footer_rules', $css_footer);
		add_post_meta($post_ID, 'custom_js_header_rules', $js_header, true) or update_post_meta($post_ID, 'custom_js_header_rules', $js_header);		
		add_post_meta($post_ID, 'custom_js_footer_rules', $js_footer, true) or update_post_meta($post_ID, 'custom_js_footer_rules', $js_footer);
	}

	function wp_header_css_jss(){
		self::insert_rules('custom_css_header_rules');
		self::insert_rules('custom_js_header_rules');
	}

	function wp_footer_css_jss(){
		self::insert_rules('custom_css_footer_rules');
		self::insert_rules('custom_js_footer_rules');
	}

	function insert_rules($key) {
		echo '<!-- Custom CSS JS (custom rules) -->';
		$rules = get_option($key);
		self::echo_rules($rules);
		if (self::is_post()) {
			global $post;
			//echo $key.'<br>';
			self::echo_rules(get_post_meta($post->ID, $key));
		}
	}

	function echo_rules($rules) {
		echo self::get_rules($rules);
	}

	function get_rules($rules) {
		if (!is_array($rules) && $rules != '' && $rules != null) {
			return $rules;
		} elseif (is_array($rules) && count($rules) && $rules[0] != '') {
			return $rules[0];
		}
	}

	function is_post() {
		return is_single() || is_page() || ((is_front_page() || is_home()) && get_option('show_on_front') == 'page');
	}
}

$custom_css_js = new Custom_CSS_JS();